import React, { useEffect, useState } from "react"
import { FetchWrapper } from "./fetch-wrapper"

function AddAutomobileForm() {
    const [models, setVehModels] = useState([])
    const [color, setColor] = useState('')
    const [year, setYear] = useState('')
    const [vin, setVin] = useState('')
    const [model, setVehModel] = useState('')

    const InventoryAPI = new FetchWrapper('http://localhost:8100/')

    const fetchData = async () => {
        const vehModelData = await InventoryAPI.get('api/models/')
        setVehModels(vehModelData.models)
    }

    useEffect(() => {
        fetchData();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const body = {}

        body.color = color
        body.year = year
        body.vin = vin
        body.model_id = model

        const newAutomobile = await InventoryAPI.post('api/automobiles/', body)
        setColor('')
        setYear('')
        setVin('')
        setVehModel('')
    }

    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const handleYearChange = (event) => {
        const value = event.target.value
        setYear(value)
    }

    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }

    const handleModelChange = (event) => {
        const value = event.target.value
        setVehModel(value)
    }

    return (
    <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add an Automobile</h1>
                    <form onSubmit={handleSubmit} id="create-automobile-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} placeholder="Color..." required type="text" value={color} name="color" id="color" className="form-control" />
                            <label htmlFor="name">Color...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleYearChange} placeholder="Year..." required type="text" value={year} name="year" id="year" className="form-control" />
                            <label htmlFor="year">Year...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} placeholder="VIN..." required type="text" value={vin} name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin">VIN...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleModelChange} required value={model} name="model" id="model" className="form-select">
                                <option value="">Choose a model...</option>
                                {models.map(model => {
                                    return(
                                        <option key={model.id} value={model.id}>
                                            {model.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </>
    )
}

export default AddAutomobileForm
