import React, { useEffect, useState } from 'react'
import { FetchWrapper } from './fetch-wrapper'

function AddCustomerForm() {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [address, setAddress] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')

    const SalespersonAPI = new FetchWrapper('http://localhost:8090/')

    const handleSubmit = async (event) => {
        event.preventDefault()
        const body = {}
        
        body.first_name = firstName
        body.last_name = lastName
        body.address = address
        body.phone_number = phoneNumber
        
        const newSalesperson = await SalespersonAPI.post('api/customers/', body)
        setFirstName('')
        setLastName('')
        setAddress('')
        setPhoneNumber('')
    }
    
    const handleFirstNameChange = (event) => {
        const value = event.target.value
        setFirstName(value)
    }
    
    const handleLastNameChange = (event) => {
        const value = event.target.value
        setLastName(value)
    }
    
    const handleAddressChange = (event) => {
        const value = event.target.value
        setAddress(value)
    }

    const handlePhoneNumberChange = (event) => {
        const value = event.target.value
        setPhoneNumber(value)
    }
    

    return (
    <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Customer</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFirstNameChange} placeholder="First name..." required type="text" value={firstName} name="first-name" id="first-name" className="form-control" />
                            <label htmlFor="first-name">First name...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleLastNameChange} placeholder="Last name..." required type="text" value={lastName} name="last-name" id="last-name" className="form-control" />
                            <label htmlFor="last-name">Last name...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleAddressChange} placeholder="Address..." required type="text" value={address} name="address" id="address" className="form-control" />
                            <label htmlFor="address">Address...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePhoneNumberChange} placeholder="Phone number..." required type="text" value={phoneNumber} name="phone-number" id="phone-number" className="form-control" />
                            <label htmlFor="phone-number">Phone number...</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </>
    )
}

export default AddCustomerForm
