import React, { useState, useEffect } from "react"
import { FetchWrapper } from "./fetch-wrapper"

function AddModelForm() {
    const [manufacturers, setManufacturers] = useState([])
    const [modelName, setModelName] = useState('')
    const [pictureUrl, setPictureUrl] = useState('')
    const [manufacturerId, setManufacturer] = useState('')

    const InventoryAPI = new FetchWrapper('http://localhost:8100/')

    const fetchData = async () => {
        const manufacturerData = await InventoryAPI.get('api/manufacturers/')
        setManufacturers(manufacturerData.manufacturers)
    }

    useEffect(() => {
        fetchData();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const body = {}

        body.name = modelName
        body.picture_url = pictureUrl
        body.manufacturer_id = manufacturerId

        const newModel = await InventoryAPI.post('api/models/', body)
        setModelName('')
        setPictureUrl('')
        setManufacturer('')
    }

    const handleModelNameChange = (event) => {
        const value = event.target.value
        setModelName(value)
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value
        setPictureUrl(value)
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }



    return (
    <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Vehicle Model</h1>
                    <form onSubmit={handleSubmit} id="create-model-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleModelNameChange} placeholder="Model Name..." required type="text" value={modelName} name="model-name" id="model-name" className="form-control" />
                            <label htmlFor="modelName">Model Name...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureUrlChange} placeholder="Picture URL..." required type="text" value={pictureUrl} name="picture-url" id="picture-url" className="form-control" />
                            <label htmlFor="picture_url">Picture URL...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleManufacturerChange} required value={manufacturerId} name="manufacturer" id="manufacturer" className="form-select">
                                <option value="">Choose a manufacturer...</option>
                                {manufacturers.map(manufacturer => {
                                    return(
                                        <option key={manufacturer.id} value={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </>
    )
}

export default AddModelForm
