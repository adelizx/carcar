import React, { useEffect, useState } from "react"
import { FetchWrapper } from "./fetch-wrapper"


function AllCurrentServicesList() {
    const [allServices, setAllServices] = useState([])
    const [allAutomobiles, setAllAutomobiles] = useState([])

    const ServiceApi = new FetchWrapper('http://localhost:8080/')
    const InventoryAPI = new FetchWrapper('http://localhost:8100/')

    const fetchData = async () => {
        const servicesData = await ServiceApi.get('api/appointments/')
        const automobileData = await InventoryAPI.get('api/automobiles/')
        setAllServices(servicesData.appointments)
        setAllAutomobiles(automobileData.autos)
    }

    useEffect(() => {
        fetchData();
    }, [])

    const cancelAppointment = async (id) => {
        const response = await ServiceApi.put(`api/appointments/${id}/cancel/`)
        setAllServices([...allServices].map(service => {
            if (service.id == id) {
                service.status = "canceled"
                return service
            } else {
                return service
            }
        }))
    }

    const finishAppointment = async (id) => {
        const response = await ServiceApi.put(`api/appointments/${id}/finish/`)
        setAllServices([...allServices].map(service => {
            if (service.id == id) {
                service.status = "finished"
                return service
            } else {
                return service
            }
        }))
    }

    const vipStatus = (id) => {
        return allAutomobiles.map(autos => (autos.vin)).includes(id)
    }

    return (
    <>
        <div className="container text-center">
            <h1>Current Appointments</h1>
        </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer Name</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Service Reason</th>
                    <th>Update Status</th>
                </tr>
            </thead>
            <tbody>
                {allServices.filter(appointments => appointments.status == "created").map(appointments => {
                    return (
                        <tr key={appointments.id}>
                            <td>{appointments.vin}</td>
                            {(vipStatus(appointments.vin)) ? <td>Yes</td> : <td>No</td>}
                            <td>{appointments.customer}</td>
                            <td>{new Date(appointments.date_time).toLocaleDateString()}</td>
                            <td>{new Date(appointments.date_time).toLocaleTimeString([], {hour: '2-digit', minute:'2-digit', timeZone: 'UTC'})}</td>
                            <td>{appointments.technician.first_name} {appointments.technician.last_name}</td>
                            <td>{appointments.reason}</td>
                            <td>
                                <button onClick={() => cancelAppointment(appointments.id)} type="button" className="btn btn-danger">Cancel</button>
                                <button onClick={() => finishAppointment(appointments.id)} type="button" className="btn btn-success">Finished</button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </>
    );
}

export default AllCurrentServicesList
