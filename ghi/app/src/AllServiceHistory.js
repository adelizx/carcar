import React, { useEffect, useState } from "react"
import { FetchWrapper } from "./fetch-wrapper"


function ServiceHistory() {
    const [allServices, setAllServices] = useState([])
    const [allAutomobiles, setAllAutomobiles] = useState([])
    const [searchInput, setSearchInput] = useState('')

    const ServiceApi = new FetchWrapper('http://localhost:8080/')
    const InventoryAPI = new FetchWrapper('http://localhost:8100/')

    const fetchData = async () => {
        const servicesData = await ServiceApi.get('api/appointments/')
        const automobileData = await InventoryAPI.get('api/automobiles/')
        setAllServices(servicesData.appointments)
        setAllAutomobiles(automobileData.autos)
    }

    useEffect(() => {
        fetchData();
    }, [])

    const vipStatus = (id) => {
        return allAutomobiles.map(autos => {
            return(autos.vin)}).includes(id)
    }

    const handleSearchChange = (event) => {
        const value = event.target.value.toUpperCase()
        setSearchInput(value)
    }

    return (
    <>
        <div className="container text-center">
            <h1>Service History</h1>
        </div>
        <div>
            <input onChange={handleSearchChange} placeholder="Search by VIN..." name="searchInput" id="searchInput" type="text" value={searchInput} />
        </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer Name</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Service Reason</th>
                    <th>Service Status</th>
                </tr>
            </thead>
            <tbody>
                {allServices.filter(appointment => {
                    if (searchInput === '') {
                        return appointment;
                    } else if (appointment.vin.toUpperCase().includes(searchInput)) {
                        return appointment;
                    }
                }).map((appointments) => (
                    <tr key={appointments.id}>
                        <td>{appointments.vin.toUpperCase()}</td>
                        {(vipStatus(appointments.vin)) ? <td>Yes</td> : <td>No</td>}
                        <td>{appointments.customer}</td>
                        <td>{new Date(appointments.date_time).toLocaleDateString()}</td>
                        <td>{new Date(appointments.date_time).toLocaleTimeString([], {hour: '2-digit', minute:'2-digit', timeZone: 'UTC'})}</td>
                        <td>{appointments.technician.first_name} {appointments.technician.last_name}</td>
                        <td>{appointments.reason}</td>
                        <td>{appointments.status}</td>
                    </tr>
                    )
                )}
            </tbody>
        </table>
    </>
    );
}

export default ServiceHistory
