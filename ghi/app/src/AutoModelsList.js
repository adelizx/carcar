import { useEffect, useState } from "react";
import { FetchWrapper } from "./fetch-wrapper";

function AutoModelsList() {
    const [allManufacturers, setAllManufacturers] = useState([])
    const [allModels, setAllModels] = useState([])
    const [modelData, setModelData] = useState([])
    const [select, setSelect] = useState('')

    const InventoryAPI = new FetchWrapper('http://localhost:8100/')

    const fetchData = async () => {
        const manufacturerData = await InventoryAPI.get('api/manufacturers/')
        const modelData = await InventoryAPI.get('api/models/')
        setAllManufacturers(manufacturerData.manufacturers)
        setAllModels(modelData.models)
        setModelData(modelData.models)
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleManufacturerChange = (event) => {
        const value = event.target.value
        setSelect(value)
        if (value) {
            setModelData([...allModels].filter(model => {
                return model.manufacturer.id === Number(value)
            }))
        } else {
            setModelData([...allModels])
        }
    }

    return (
    <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h3>Select a Manufacturer</h3>
                    <div className="mb-3">
                        <select onChange={handleManufacturerChange} value={select} name="select" id="select" className="form-select">
                            <option value="">All manufacturers in inventory...</option>
                            {allManufacturers.map(manufacturer => {
                                return (
                                    <option key={manufacturer.id} value={manufacturer.id}>
                                        {manufacturer.name}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <h2>Car Models</h2>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Make</th>
                    <th>Model</th>
                    <th>Picture URL</th>
                </tr>
            </thead>
            <tbody>
                {modelData.map(model => {
                    return (
                        <tr key={model.id}>
                            <td>{ model.manufacturer.name }</td>
                            <td>{ model.name }</td>
                            <td>{ model.picture_url }</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </>
    );
}

export default AutoModelsList
