import React, { useEffect, useState } from "react"
import { FetchWrapper } from "./fetch-wrapper"

function AutomobileList() {
    const [allAutomobiles, setAllAutomobiles] = useState([])

    const InventoryAPI = new FetchWrapper('http://localhost:8100/')

    const fetchData = async () => {
        const automobileData = await InventoryAPI.get('api/automobiles/')
        setAllAutomobiles(automobileData.autos)
    }

    useEffect(() => {
        fetchData()
    }, [])


    return (
    <>
        <div className="container text-center">
            <h1>Automobiles</h1>
        </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                    <th>Sold</th>
                </tr>
            </thead>
            <tbody>
                {allAutomobiles.map(autos => {
                    return (
                        <tr key={autos.id}>
                            <td>{ autos.vin }</td>
                            <td>{ autos.color}</td>
                            <td>{ autos.year}</td>
                            <td>{ autos.model.name}</td>
                            <td>{ autos.model.manufacturer.name}</td>
                            {(autos.sold) ? <td>Sold!</td> : <td>Available</td>}
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </>
    );
}

export default AutomobileList
