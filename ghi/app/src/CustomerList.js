import { useEffect, useState } from "react";
import { FetchWrapper } from "./fetch-wrapper";

function CustomerList() {
    const [allCustomers, setAllCustomers] = useState([])

    const CustomerAPI = new FetchWrapper('http://localhost:8090/')

    const fetchData = async () => {
        const customerData = await CustomerAPI.get('api/customers/')
        setAllCustomers(customerData.customers)
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
    <>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Phone Number</th>
                    <th>Address</th>
                </tr>
            </thead>
            <tbody>
                {allCustomers.map(customer => {
                    return (
                        <tr key={customer.id}>
                            <td>{ customer.first_name }</td>
                            <td>{ customer.last_name }</td>
                            <td>{ customer.phone_number }</td>
                            <td>{ customer.address }</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </>
    );
}

export default CustomerList
