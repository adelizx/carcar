import { useEffect, useState } from "react";
import { FetchWrapper } from "./fetch-wrapper";

function ManufacturerList() {
    const [allManufacturers, setAllManufacturers] = useState([])

    const InventoryAPI = new FetchWrapper('http://localhost:8100/')

    const fetchData = async () => {
        const manufacturersData = await InventoryAPI.get('api/manufacturers/')
        setAllManufacturers(manufacturersData.manufacturers)
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
    <>
    <div className="container text-center">
        <h1>Manufacturers</h1>
        <div className="row">
                {allManufacturers.map(manufacturer => {
                    return (
                    <div key={manufacturer.id} className="col gy-4">
                        <div className="card" style={{width: 18 + 'rem'}}>
                            <div className="card-body">
                                <h5 className="card-title">{ manufacturer.name }</h5>
                                <p className="card-text">Inventory href: { manufacturer.href }</p>
                            </div>
                        </div>
                    </div>
                    )
                })}
        </div>
    </div>
    </>
    );
}

export default ManufacturerList
