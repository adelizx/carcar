import React, { useEffect, useState } from 'react'
import { FetchWrapper } from './fetch-wrapper'

function RecordNewSale() {
    const [autos, setAutos] = useState([])
    const [salespeople, setSalespeople] = useState([])
    const [customers, setCustomers] = useState([])

    const [vin, setVin] = useState('')
    const [salesperson, setSalesperson] = useState('')
    const [customer, setCustomer] = useState('')
    const [price, setPrice] = useState('')
    
    const SalesAPI = new FetchWrapper('http://localhost:8090/')
    const InventoryAPI = new FetchWrapper('http://localhost:8100/')

    const fetchData = async () => {
        const automobilesData = await SalesAPI.get('api/automobiles/')
        const salespeopleData = await SalesAPI.get('api/salespeople/')
        const customerData = await SalesAPI.get('api/customers/')
        setAutos([...automobilesData.automobiles].filter(car => car.sold === false))
        setSalespeople(salespeopleData.salespeople)
        setCustomers(customerData.customers)
    }

    useEffect(() => {
        fetchData();
    }, [])
    
    const handleSubmit = async (event) => {
        event.preventDefault()
        const body = {}
        body.price = price
        body.automobile = vin
        body.salesperson = salesperson
        body.customer = customer
        const update = {}
        update.sold = true
        const url = 'http://localhost:8090/api/sales/'

        try {
            const response = await fetch(url, {
                method: "POST",
                body: JSON.stringify(body),
                headers: {
                    "Content-Type": "application/json"
                }
            })
            if (response.ok) {
                const carVOSold = await SalesAPI.put(`api/automobiles/${vin}/`, update)
                const inventoryCarSold = await InventoryAPI.put(`api/automobiles/${vin}/`, update)
                setAutos([...autos].filter(car => car.vin !== vin))
                setVin('')
                setSalesperson('')
                setCustomer('')
                setPrice('')
            } else {
                throw new Error('Response not "OK"')
            }
        } catch (error) {
            console.error('error', error)
        }
    }

    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }

    const handleSalespersonChange = (event) => {
        const value = event.target.value
        setSalesperson(value)
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value
        setCustomer(value)
    }

    const handlePriceChange = (event) => {
        const value = event.target.value
        setPrice(value)
    }

    return (
        <>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Record a new sale</h1>
                        <form onSubmit={handleSubmit} id="record-sale-form">
                            <div className="mb-3">
                                <select onChange={handleVinChange} required value={vin} name="vin" id="vin" className="form-select">
                                    <option value="">Choose an automobile VIN...</option>
                                    {autos.map(car => {
                                        return (
                                            <option key={car.id} value={car.vin}>
                                                {car.vin}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleSalespersonChange} required value={salesperson} name="salesperson" id="salesperson" className="form-select">
                                    <option value="">Choose a salesperson</option>
                                    {salespeople.map(salesperson => {
                                        return (
                                            <option key={salesperson.id} value={salesperson.employee_id}>
                                                {salesperson.first_name} {salesperson.last_name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleCustomerChange} required value={customer} name="customer" id="customer" className="form-select">
                                    <option value="">Choose a customer</option>
                                    {customers.map(customer => {
                                        return (
                                            <option key={customer.id} value={customer.id}>
                                                {customer.first_name} {customer.last_name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handlePriceChange} placeholder="Price" required type="text" value={price} name="price" id="price" className="form-control" />
                                <label htmlFor="price">Price</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}

export default RecordNewSale
