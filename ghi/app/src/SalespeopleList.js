import { useEffect, useState } from "react";
import { FetchWrapper } from "./fetch-wrapper";

function SalespersonList() {
    const [allSalespeople, setAllSalespeople] = useState([])

    const SalespersonAPI = new FetchWrapper('http://localhost:8090/')

    const fetchData = async () => {
        const salespeopleData = await SalespersonAPI.get('api/salespeople/')
        setAllSalespeople(salespeopleData.salespeople)
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
    <>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
                {allSalespeople.map(salesperson => {
                    return (
                        <tr key={salesperson.id}>
                            <td>{ salesperson.employee_id }</td>
                            <td>{ salesperson.first_name }</td>
                            <td>{ salesperson.last_name }</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </>
    );
}

export default SalespersonList
