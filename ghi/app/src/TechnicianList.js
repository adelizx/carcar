import { useEffect, useState } from "react";
import { FetchWrapper } from "./fetch-wrapper";

function TechnicianList() {
    const [allTechnicians, setAllTechnicians] = useState([])

    const ServiceApi = new FetchWrapper ('http://localhost:8080/')

    const fetchData = async () => {
        const technicianData = await ServiceApi.get('api/technicians/')
        setAllTechnicians(technicianData.Technicians)
    }

    useEffect(() => {
        fetchData()
    }, [])


    return (
    <>
        <div className="container text-center">
            <h1>Technicians</h1>
        </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
                {allTechnicians.map(Technicians => {
                    return (
                        <tr key={Technicians.id}>
                            <td>{ Technicians.employee_id }</td>
                            <td>{ Technicians.first_name }</td>
                            <td>{ Technicians.last_name }</td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </>
    );
}

export default TechnicianList
