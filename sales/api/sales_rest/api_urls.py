from django.urls import path
from .views import (
    api_list_salespeople,
    delete_salesperson,
    api_list_customers,
    delete_customer,
    api_list_sales,
    delete_sale,
    api_list_automobiles,
)


urlpatterns = [
    path("salespeople/", api_list_salespeople, name="api_list_salespeople"),
    path("salespeople/<int:id>/", delete_salesperson, name="delete_salesperson"),  # noqa
    path("customers/", api_list_customers, name="api_list_customers"),
    path("customers/<int:id>/", delete_customer, name="delete_customer"),
    path("sales/", api_list_sales, name="api_list_sales"),
    path("sales/<int:id>/", delete_sale, name="delete_sale"),
    path("automobiles/", api_list_automobiles, name="api_list_automobiles"),
    path("automobiles/<str:vin>/", api_list_automobiles, name="api_update_automobile"),  # noqa
]
