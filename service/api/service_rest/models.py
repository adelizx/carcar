from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField()


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100, unique=True)

    def get_api_url(self):
        return reverse("show_tech", kwargs={"id": self.id})

    class Meta:
        ordering = ("employee_id",)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=200, default="created")
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=200)

    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )

    def finish(self):
        status = "finished"
        self.status = status
        self.save()

    def cancel(self):
        status = "canceled"
        self.status = status
        self.save()

    def __str__(self):
        return self.customer

    def get_api_url(self):
        return reverse("show_appt", kwargs={"id": self.id})

    class Meta:
        ordering = ("date_time",)
