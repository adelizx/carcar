from django.urls import path
from .views import (
    api_list_techs,
    api_list_appointments,
    api_show_tech,
    api_show_appt,
    cancel_appt,
    finish_appt,
)

urlpatterns = [
    path("technicians/", api_list_techs, name="list_techs"),
    path("technicians/<int:id>/", api_show_tech, name="show_tech"),
    path("appointments/", api_list_appointments, name="list_appointments"),
    path("appointments/<int:id>/", api_show_appt, name="show_appt"),
    path("appointments/<int:id>/finish/", finish_appt, name="finish_appt"),
    path("appointments/<int:id>/cancel/", cancel_appt, name="cancel_appt"),
]
