from .models import AutomobileVO, Technician, Appointment
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold"]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "employee_id",
        "first_name",
        "last_name",
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
    ]
    encoders = {"technician": TechnicianListEncoder()}


@require_http_methods(["GET", "POST"])
def api_list_techs(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"Technicians": technicians},
            encoder=TechnicianListEncoder,
        )
    else:
        content = json.loads(request.body)
        tech = Technician.objects.create(**content)
        return JsonResponse(
            tech,
            encoder=TechnicianListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_show_tech(request, id):
    if request.method == "DELETE":
        try:
            technician = Technician.objects.get(pk=id)
            technician.delete()
            return JsonResponse({"message": "Technician has been deleted"})
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician does not exist"}, status=404)


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appt = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appt},
            encoder=AppointmentListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician_id = content["technician"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician
            appt = Appointment.objects.create(**content)
            return JsonResponse(
                {"appointments": appt},
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Could not create appointment"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def api_show_appt(request, id):
    if request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.delete()
            return JsonResponse({"message": "Appointment has been deleted"})
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist"}, status=404)


@require_http_methods(["PUT"])
def finish_appt(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.finish()
    return JsonResponse(
        appointment,
        encoder=AppointmentListEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def cancel_appt(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.cancel()
    return JsonResponse(
        appointment,
        encoder=AppointmentListEncoder,
        safe=False,
    )
